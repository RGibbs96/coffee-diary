from django.http import HttpResponse

def health_endpoint(request):
    if request.method == "GET":
        return HttpResponse('ok')
